package main.java.vista.controladores;

import com.jfoenix.controls.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import main.java.controlador.AppVideo;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VentanaRegistroController implements Initializable {

    @FXML
    StackPane stackPane;

    @FXML
    GridPane gridPane;

    @FXML
    JFXTextField campoNombre;

    @FXML
    JFXTextField campoApellidos;

    @FXML
    JFXTextField campoCorreo;

    @FXML
    JFXTextField campoUsuario;

    @FXML
    JFXPasswordField campoPassword;

    @FXML
    JFXPasswordField campoRepeat;

    @FXML
    JFXButton botonVolverLogin;

    @FXML
    JFXDatePicker campoFecha;

    @FXML
    JFXButton botonRegistro;


    private static final Pattern CORREO_VALIDO = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    AppVideo controlador = AppVideo.getUnicaInstancia();

    public void initialize(URL location, ResourceBundle resources) {
        KeyCombination play = new KeyCodeCombination(KeyCode.ENTER);
        this.stackPane.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (play.match(event)) {
                    botonRegistro.fire();
                }
            }
        });
    }

    private void volverLogin(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java/vista/fxml/VentanaLogin.fxml"));
        Parent parent = loader.load();
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(new Scene(parent, 874, 557));
    }

    @FXML
    private void yaTengoCuenta(ActionEvent event) throws IOException {
        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(200), ae -> {
            try {
                volverLogin(event);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
        timeline.play();
    }

    private boolean validarCorreo(String email) {
        Matcher matcher = CORREO_VALIDO.matcher(email);
        return matcher.find();
    }

    @FXML
    private void registrarse(ActionEvent event) throws IOException {
        boolean registroValido = true;
        String error = "The following fields are incorrect or empty: \r\n";
        error = error + "\r\n";

        if(this.campoNombre.getText().equals("")){
            registroValido = false;
            error = error + "- Name \r\n";
        }
        if(this.campoApellidos.getText().equals("")){
            registroValido = false;
            error = error + "- Surname \r\n";
        }
        if(this.campoCorreo.getText().equals("")){
            registroValido = false;
            error = error + "- Email \r\n";
        }
        if(!validarCorreo(this.campoCorreo.getText())){
            registroValido = false;
            error = error + "- Your email is not valid \r\n";
        }
        if(this.campoUsuario.getText().equals("")){
            registroValido = false;
            error = error + "- Username \r\n";
        }
        if(this.campoPassword.getText().equals("")){
            registroValido = false;
            error = error + "- Password \r\n";
        }
        if(this.campoRepeat.getText().equals("")){
            registroValido = false;
            error = error + "- Repeat-password \r\n";
        }
        if(!this.campoPassword.getText().equals(this.campoRepeat.getText())){
            registroValido = false;
            error = error + "- Passwords are different \r\n";
        }
        if(this.campoFecha.getValue() == null){
            registroValido = false;
            error = error + "- Date not valid.";
        }

        if(registroValido){

            if (AppVideo.getUnicaInstancia().registrarUsuario(this.campoNombre.getText(), this.campoApellidos.getText(),
                    this.campoCorreo.getText(), this.campoUsuario.getText(), this.campoPassword.getText(),
                    this.campoFecha.getValue())) {

                JFXDialogLayout cuadroDialog = new JFXDialogLayout();
                cuadroDialog.setHeading(new Text("Register succesful. Welcome to AppVideo!"));

                cuadroDialog.setBody(new Text("You can change any personal information later, in your profile section." +
                        " We hope you enjoy our app!"));
                JFXDialog dialog = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

                JFXButton button = new JFXButton("Continue");
                button.setBackground(new Background(new BackgroundFill(Color.PALEVIOLETRED,null,null)));
                button.setTextFill(Color.WHITE);
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        dialog.close();
                        try {
                            volverLogin(event);
                        } catch (Exception e) {
                        }
                    }
                });
                cuadroDialog.setActions(button);
                dialog.show();
            }
        }
        else{
            JFXDialogLayout cuadroDialog = new JFXDialogLayout();
            JFXDialog dialog = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);
            cuadroDialog.setHeading(new Text("Register failed."));
            cuadroDialog.setBody(new Text(error));

            JFXButton button = new JFXButton("Got it");
            button.setBackground(new Background(new BackgroundFill(Color.DARKORANGE,null,null)));
            button.setTextFill(Color.WHITE);
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                    try {
                        dialog.close();
                    } catch (Exception e) {
                    }
                }
            });
            cuadroDialog.setActions(button);
            dialog.show();
        }
    }
}
