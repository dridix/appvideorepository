package main.java.vista.controladores;

import com.jfoenix.controls.*;
import javafx.collections.ObservableList;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import main.java.controlador.AppVideo;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import main.java.model.CatalogoVideos;
import main.java.model.ListaVideos;
import main.java.model.Video;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VentanaPerfilController implements Initializable {

    // Elementos comunes a todas las secciones.

    @FXML
    private StackPane stackPane;

    @FXML
    private StackPane stackCentral;

    @FXML
    private GridPane gridExterior;

    @FXML
    private JFXButton botonMainPage;

    @FXML
    private JFXButton botonListas;

    @FXML
    private JFXButton botonRecientes;

    @FXML
    private JFXButton botonConfig;

    @FXML
    private JFXButton botonPremium;

    @FXML
    private JFXButton botonLogout;

    // Sección Main Page

    @FXML
    private GridPane gridMain;

    @FXML
    private Label textoConNick;

    @FXML
    private JFXComboBox<String> opcionesFiltros;

    @FXML
    private Text nombre;

    @FXML
    private Text apellido;

    @FXML
    private Text correo;

    @FXML
    private Text fechaCumple;

    @FXML
    private Text numVistos;

    @FXML
    private Text fechaRegistro;

    @FXML
    private JFXButton generatePDF;

    @FXML
    private JFXButton saveFilter;

    // Sección Config

    @FXML
    private GridPane gridConfig;

    @FXML
    private JFXTextField configNombre;

    @FXML
    private JFXTextField configApellido;

    @FXML
    private JFXTextField configCorreo;

    @FXML
    private JFXDatePicker configFecha;

    @FXML
    private JFXButton botonGuardarConfig;

    // Sección Lists

    @FXML
    private GridPane gridListas;

    @FXML
    private TableView<ListaVideos> tablaListas;

    // Sección Most Watched

    @FXML
    private GridPane gridMasvistos;

    @FXML
    private TableView<Video> tablaMasvistos;

    // Sección Recents

    @FXML
    private GridPane gridRecientes;

    @FXML
    private TableView<Video> tablaRecientes;

    // Sección Premium

    @FXML
    private GridPane gridPremium;

    @FXML
    private JFXButton upgradeToPremium;

    @FXML
    private JFXButton cancelPremium;


    private String gridActual;
    private AppVideo controlador = AppVideo.getUnicaInstancia();
    private static final Pattern EMAIL_VALIDO = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);


    @Override
    public void initialize(URL location , ResourceBundle resources) {

        if(!controlador.getUsuarioActual().isPremium()){
            controlador.setTipoFiltro(controlador.getUsuarioActual(), "NoFiltro");
        }
        else{
            opcionesFiltros.setVisible(true);
            generatePDF.setVisible(true);
            saveFilter.setVisible(true);
            upgradeToPremium.setVisible(false);
            cancelPremium.setVisible(true);
            opcionesFiltros.setPromptText(controlador.getUsuarioActual().getTipoFiltro().getValor());
        }

        gridActual = "mainpage";
        prepareMainPage();
        setUsername();
        setNumVistos();
        setFechaRegistro();
        preparePrompText();
        addOpcionesFiltros();
    }

    private void setUsername() {
        textoConNick.setText(controlador.getUsuarioActual().getNombreUsuario());
        textoConNick.setFont(Font.font("Futura", 28));
        textoConNick.setTextFill(Color.valueOf("#D48C20"));
    }

    private void setNumVistos() {
        int x = controlador.getUsuarioActual().getNumVideosVistos();
        numVistos.setText(Integer.toString(x));
    }

    private void setFechaRegistro() {
        fechaRegistro.setText(controlador.getUsuarioActual().getFechaRegistro().toString());
    }

    private void prepareMainPage() {
        nombre.setText(controlador.getUsuarioActual().getNombre());
        apellido.setText(controlador.getUsuarioActual().getApellidos());
        correo.setText(controlador.getUsuarioActual().getEmail());
        fechaCumple.setText(controlador.getUsuarioActual().getFechaNacimiento().toString());
    }

    // Funciones generales del grid exterior.

    @FXML
    private void goVentanaPrincipal(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java/vista/fxml/VentanaPrincipal.fxml"));
        Parent parent = loader.load();
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(new Scene(parent, 900, 615));
    }

    private void cambiarGrid() {
        switch (gridActual) {
            case "mainpage":
                gridMain.setVisible(false);
                break;
            case "lists":
                gridListas.setVisible(false);
                break;
            case "mostwatched":
                gridMasvistos.setVisible(false);
                break;
            case "recents":
                gridRecientes.setVisible(false);
                break;
            case "config":
                gridConfig.setVisible(false);
                break;
            case "premium":
                gridPremium.setVisible(false);
                break;
        }
    }

    @FXML
    private void goMainPage(ActionEvent event) {
        if(!gridActual.equals("mainpage")){
            cambiarGrid();
            gridActual = "mainpage";
            prepareMainPage();
            gridMain.setVisible(true);
        }
    }

    @FXML
    private void goLists(ActionEvent event) {
        tablaListas.getColumns().clear();
        tablaListas.getItems().clear();
        cargarListas();
        if(!gridActual.equals("lists")){
            cambiarGrid();
            gridActual = "lists";
            gridListas.setVisible(true);
        }
    }

    @FXML
    private void goMostWatched(ActionEvent event) {
        tablaMasvistos.getColumns().clear();
        tablaMasvistos.getItems().clear();
        cargarMasVistos();
        if(!gridActual.equals("mostwatched")){
            cambiarGrid();
            gridActual = "mostwatched";
            gridMasvistos.setVisible(true);
        }
    }

    @FXML
    private void goRecents(ActionEvent event) {
        tablaRecientes.getColumns().clear();
        tablaRecientes.getItems().clear();
        cargarRecientes();
        if(!gridActual.equals("recents")){
            cambiarGrid();
            gridActual = "recents";
            gridRecientes.setVisible(true);
        }
    }

    @FXML
    private void goConfig(ActionEvent event) {
        if(!gridActual.equals("config")){
            cambiarGrid();
            gridActual = "config";
            gridConfig.setVisible(true);
        }
    }

    @FXML
    private void goPremium(ActionEvent event) {
        if(!gridActual.equals("premium")){
            cambiarGrid();
            gridActual = "premium";
            gridPremium.setVisible(true);
        }
    }

    @FXML
    private void logout(ActionEvent event) {
        Stage aux = (Stage) stackPane.getScene().getWindow();
        aux.close();
        controlador.logout();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java/vista/fxml/VentanaLogin.fxml"));

        Parent root;
        try {
            root = loader.load();
            Scene scene = new Scene(root);
            Stage appStage = new Stage();
            appStage.setMinWidth(804);
            appStage.setMinHeight(508);
            appStage.setWidth(874);
            appStage.setHeight(578);
            appStage.setMaxWidth(944);
            appStage.setMaxHeight(648);
            appStage.setResizable(true);
            appStage.setScene(scene);
            appStage.show();

            appStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    Platform.exit();
                    System.exit(0);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Funciones de la sección "Main Page"

    @FXML
    private void generarPDF(ActionEvent event) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Select the folder where your lists-pdf will be saved.");
        File defaultDirectory = new File("/");
        chooser.setInitialDirectory(defaultDirectory);
        File ruta = chooser.showDialog(null);
        if(ruta != null) {
            controlador.generarPDF(controlador.getUsuarioActual(),ruta.getAbsolutePath());
        }
    }

    private void addOpcionesFiltros() {

        List<Video> catalogoVideos = CatalogoVideos.getUnicaInstancia().getVideos();

        ObservableList<String> generosFiltros = FXCollections.observableArrayList();

        for (Video video : catalogoVideos) {
            if(!generosFiltros.contains(video.getGenero())){
                generosFiltros.add(video.getGenero());
            }
        }

        generosFiltros.add("NoFiltro");
        opcionesFiltros.setItems(generosFiltros);
    }

    @FXML
    private void elegirFiltro() {

        controlador.setTipoFiltro(controlador.getUsuarioActual(), opcionesFiltros.getSelectionModel().getSelectedItem());
        opcionesFiltros.setPromptText(opcionesFiltros.getSelectionModel().getSelectedItem());

        JFXDialogLayout cuadroDialog = new JFXDialogLayout();
        cuadroDialog.setHeading(new Text("Filter selected! \r\n\r\n"));

        cuadroDialog.setBody(new Text("The videos with genre -> " + opcionesFiltros.getSelectionModel().getSelectedItem() + " will NOT appear in your future searchs."));
        JFXDialog dialog = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

        JFXButton button = new JFXButton("Got it");
        button.setBackground(new Background(new BackgroundFill(Color.DARKVIOLET,null,null)));
        button.setTextFill(Color.WHITE);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
                try {
                    goMainPage(event);
                } catch (Exception e) {
                }
            }
        });
        cuadroDialog.setActions(button);
        dialog.show();
    }

    // Funciones de la sección "Config"

    private boolean validarMail(String email) {
        Matcher matcher = EMAIL_VALIDO.matcher(email);
        return matcher.find();
    }

    @FXML
    private void guardarCambiosConfig(ActionEvent event) {

        String camposError = "";
        int error = 0;
        boolean correoCorrecto = true;

        if(configNombre.getText().equals("")){
            camposError = camposError + "name";
            error = 1;
        }
        if(configApellido.getText().equals("")){
            camposError = camposError + "surname";
            error = 1;

        }
        if(configCorreo.getText().equals("")){
            camposError = camposError + "email";
            error = 1;

        }
        if(!validarMail(configCorreo.getText())){
            correoCorrecto = false;
            error = 1;

        }

        if((error==0) && (correoCorrecto)){
            nombre.setText(configNombre.getText());
            apellido.setText(configApellido.getText());
            correo.setText(configCorreo.getText());
            fechaCumple.setText(configFecha.getValue().toString());

            controlador.actualizarUsuario(configNombre.getText(), configApellido.getText(),
                    configCorreo.getText(), configFecha.getValue());

            JFXDialogLayout cuadroDialog = new JFXDialogLayout();
            cuadroDialog.setHeading(new Text("Changes saved!"));
            JFXDialog dialogConfig = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

            JFXButton continuar = new JFXButton("Continue");

            continuar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    configNombre.setText("");
                    configApellido.setText("");
                    configCorreo.setText("");
                    configFecha.setValue(null);

                    goMainPage(event);
                    dialogConfig.close();
                }
            });
            cuadroDialog.setActions(continuar);
            dialogConfig.show();
        }
        else{
            JFXDialogLayout cuadroDialog = new JFXDialogLayout();
            cuadroDialog.setHeading(new Text("Ops! Maybe there's an error..."));
            JFXDialog dialogError = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

            JFXButton continuar = new JFXButton("Okay, take me back");
            continuar.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW,null,null)));
            continuar.setTextFill(Color.BLACK);

            continuar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    goConfig(event);
                    dialogError.close();
                }
            });
            cuadroDialog.setActions(continuar);
            dialogError.show();
        }
    }

    private void preparePrompText() {
        configNombre.setPromptText(nombre.getText());
        configApellido.setPromptText(apellido.getText());
        configCorreo.setPromptText(correo.getText());
        configFecha.setPromptText(fechaCumple.getText());
    }

    // Funciones de la sección "Lists"

    private void cargarListas() {

        tablaListas.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        TableColumn<ListaVideos, String> columnaNombreLista = new TableColumn<>("Name");
        TableColumn<ListaVideos, String> columnaNumVideos = new TableColumn<>("Number of videos");
        TableColumn<ListaVideos, String> columnaFechaCreacion = new TableColumn<>("Created");

        columnaNombreLista.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombreLista()));
        columnaNumVideos.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNumeroVideos()));
        columnaFechaCreacion.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFechaCreacion()));


        ObservableList<ListaVideos> listas = FXCollections.observableArrayList();
        listas.addAll(controlador.getUsuarioActual().getListasReproduccion());

        tablaListas.setItems(listas);
        tablaListas.getColumns().addAll(columnaNombreLista, columnaNumVideos, columnaFechaCreacion);
    }

    // Funciones de la sección "Most watched"

    private void cargarMasVistos() {

        tablaMasvistos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        TableColumn<Video, String> columnaNombreVideo = new TableColumn<>("Name");
        TableColumn<Video, String> columnaGenero = new TableColumn<>("Genre");
        TableColumn<Video, String> columnaNumReproducciones = new TableColumn<>("Views");

        columnaNombreVideo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTitulo()));
        columnaGenero.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getGenero()));
        columnaNumReproducciones.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getStringReproducciones()));

        ObservableList<Video> masVistos = FXCollections.observableArrayList(controlador.getMasReproducidosUsuario
                        (controlador.getUsuarioActual()));

        tablaMasvistos.setItems(masVistos);
        tablaMasvistos.getColumns().addAll(columnaNombreVideo, columnaGenero, columnaNumReproducciones);
    }


    // Funciones de la sección "Recents"

    private void cargarRecientes() {

        tablaRecientes.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        TableColumn<Video, String> columnaNombreVideo = new TableColumn<>("Name");
        TableColumn<Video, String> columnaGenero = new TableColumn<>("Genre");
        TableColumn<Video, String> columnaNumReproducciones = new TableColumn<>("Views");

        columnaNombreVideo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTitulo()));
        columnaGenero.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getGenero()));
        columnaNumReproducciones.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getStringReproducciones()));

        ObservableList<Video> recientes = FXCollections.observableArrayList(controlador.getVideosRecientes
                        (controlador.getUsuarioActual()));

        tablaRecientes.setItems(recientes);
        tablaRecientes.getColumns().addAll(columnaNombreVideo, columnaGenero, columnaNumReproducciones);
    }


    // Funciones de la sección "Premium"

    @FXML
    private void upgradePremium(ActionEvent event) {

        JFXDialogLayout cuadroDialog = new JFXDialogLayout();
        cuadroDialog.setHeading(new Text("Congratulations! You're now a premium user"));
        JFXDialog dialogPremium = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

        JFXButton continuar = new JFXButton("Continue");
        continuar.setBackground(new Background(new BackgroundFill(Color.DARKBLUE,null,null)));
        continuar.setTextFill(Color.WHITE);

        continuar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialogPremium.close();
            }
        });
        controlador.mejorarCuenta(controlador.getUsuarioActual());

        upgradeToPremium.setVisible(false);
        cancelPremium.setVisible(true);

        opcionesFiltros.setVisible(true);
        generatePDF.setVisible(true);
        saveFilter.setVisible(true);

        cuadroDialog.setActions(continuar);
        dialogPremium.show();
    }

    @FXML
    private void cancelPremium(ActionEvent event) {

        JFXDialogLayout cuadroDialog = new JFXDialogLayout();
        cuadroDialog.setHeading(new Text("You're not a premium user now. Your filter was changed to NoFiltro"));
        JFXDialog dialogPremium = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

        JFXButton continuar = new JFXButton("Okay");
        continuar.setBackground(new Background(new BackgroundFill(Color.DARKGREEN,null,null)));
        continuar.setTextFill(Color.WHITE);

        continuar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialogPremium.close();
            }
        });
        controlador.cancelarSuscripcion(controlador.getUsuarioActual());
        controlador.setTipoFiltro(controlador.getUsuarioActual(), "NoFiltro");

        upgradeToPremium.setVisible(true);
        cancelPremium.setVisible(false);

        saveFilter.setVisible(false);
        opcionesFiltros.setVisible(false);
        generatePDF.setVisible(false);

        cuadroDialog.setActions(continuar);
        dialogPremium.show();

    }

}
