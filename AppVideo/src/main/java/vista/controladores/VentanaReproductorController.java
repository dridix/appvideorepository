package main.java.vista.controladores;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import main.java.controlador.AppVideo;
import javafx.fxml.Initializable;
import main.java.dao.IntEtiquetaDAO;
import main.java.model.CatalogoEtiquetas;
import main.java.model.Etiqueta;
import main.java.model.Video;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public class VentanaReproductorController implements Initializable {

    @FXML
    private StackPane stackPane;

    @FXML
    private GridPane gridPane;

    @FXML
    private JFXButton botonAtras;

    @FXML
    private JFXTextField campoCrearEtiqueta;

    @FXML
    private JFXTextField campoBorrarEtiqueta;

    @FXML
    private FlowPane panelEtiquetas;

    @FXML
    private Label labelTitulo;

    private Video videoSeleccionado;
    private AppVideo controlador = AppVideo.getUnicaInstancia();
    private WebView webview;
    private IntEtiquetaDAO etiquetaDAO;

    @Override
    public void initialize(URL location , ResourceBundle resources) {
        videoSeleccionado = controlador.getVideoSeleccionado();
        labelTitulo.setText(videoSeleccionado.getTitulo());
        labelTitulo.setStyle("-fx-font-weight: bold;");
        cargarEtiquetas();
        reproducirVideo();


        KeyCombination play = new KeyCodeCombination(KeyCode.ENTER);
        this.stackPane.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if ((play.match(event)) && (!campoCrearEtiqueta.getText().equals(""))) {
                    addEtiqueta();
                    campoCrearEtiqueta.setText("");
                }
                if ((play.match(event)) && (!campoBorrarEtiqueta.getText().equals(""))) {
                    borrarEtiqueta();
                    campoBorrarEtiqueta.setText("");
                }
            }
        });
    }

    private void reproducirVideo() {

        String url = videoSeleccionado.getUrl();
        String urlConcreta = url.substring(url.lastIndexOf('=')+1);
        String embebido = "https://www.youtube.com/embed/";
        String urlfinal = embebido + urlConcreta;

        webview = new WebView();
        webview.getEngine().load(urlfinal);
        gridPane.add(webview, 1, 1);
    }

    private void cargarEtiquetas() {
        List<Etiqueta> etiquetas = new LinkedList<>(videoSeleccionado.getEtiquetas());

        panelEtiquetas.getChildren().clear();

        for(Etiqueta e : etiquetas) {

            Text etiqueta = new Text();
            etiqueta.setStyle("-fx-fill: yellow");
            etiqueta.setText(e.getNombre()+ "        ");
            panelEtiquetas.getChildren().add(etiqueta);
        }
    }

    private void addEtiqueta() {

        if(!CatalogoEtiquetas.getUnicaInstancia().existeEtiqueta(campoCrearEtiqueta.getText())){
            Etiqueta nuevaEtiqueta = new Etiqueta(campoCrearEtiqueta.getText());
            controlador.addEtiquetaVideo(nuevaEtiqueta, videoSeleccionado);
        }
        else{
            Etiqueta e = CatalogoEtiquetas.getUnicaInstancia().getEtiqueta(campoCrearEtiqueta.getText());
            controlador.addEtiquetaVideo(e, videoSeleccionado);
        }
        cargarEtiquetas();
    }


    private void borrarEtiqueta() {

        if(CatalogoEtiquetas.getUnicaInstancia().existeEtiqueta(campoBorrarEtiqueta.getText())){

            for(Etiqueta e : videoSeleccionado.getEtiquetas()){
                if(e.getNombre().equals(campoBorrarEtiqueta.getText())){
                    controlador.removeEtiquetaVideo(e, videoSeleccionado);
                }
            }
        }
        cargarEtiquetas();
    }



    @FXML
    private void volverPrincipal(MouseEvent event) throws IOException {

        webview.getEngine().load(null);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java/vista/fxml/VentanaPrincipal.fxml"));
        Parent parent = loader.load();
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(new Scene(parent, 900, 615));
    }

}
