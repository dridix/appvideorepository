package main.java.vista.controladores;

import com.jfoenix.controls.*;
import javafx.scene.layout.*;
import main.java.controlador.AppVideo;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.*;
import javafx.util.Duration;
import main.java.dao.UsuarioDAO;

public class VentanaLoginController implements Initializable {

    @FXML
    private StackPane stackPane;

    @FXML
    private GridPane gridPane;

    @FXML
    private JFXPasswordField campoPassword;

    @FXML
    private JFXTextField campoUsuario;

    @FXML
    private Button botonRegistro;

    @FXML
    private Button botonLogin;

    @FXML
    private Button botonCargarVideos;

    @FXML
    private Label datoIncorrecto;

    static UsuarioDAO uDAO = new UsuarioDAO();
    AppVideo controlador = AppVideo.getUnicaInstancia();

    @Override
    public void initialize(URL location , ResourceBundle resources) {

        KeyCombination play = new KeyCodeCombination(KeyCode.ENTER);
        this.stackPane.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (play.match(event)) {
                    botonLogin.fire();
                }
            }
        });
    }

    @FXML
    private void tryLogin(ActionEvent event) throws IOException{
        if(!controlador.login(this.campoUsuario.getText(), this.campoPassword.getText())){
            this.datoIncorrecto.setVisible(true);
        }else{
            Timeline timeline = new Timeline(new KeyFrame(
                    Duration.millis(300),
                    ae -> {
                        try {
                            entrarPrincipal(event);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }));
            timeline.play();
        }
    }

    private void entrarPrincipal(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/main/java/vista/fxml/VentanaPrincipal.fxml"));
        Region root = fxmlLoader.load();

        Stage lastStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        lastStage.hide();
        event.consume();

        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(new Scene(root));
        appStage.setMinWidth(900);
        appStage.setMinHeight(615);
        appStage.setWidth(900);
        appStage.setHeight(615);
        appStage.setResizable(true);
        appStage.toFront();
        appStage.show();

        appStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });
    }

    @FXML
    private void entrarRegistro(ActionEvent event) throws IOException{
        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(200),
                ae -> {
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java/vista/fxml/VentanaRegistro.fxml"));
                        Parent parent = loader.load();
                        ((Stage)stackPane.getScene().getWindow()).setScene(new Scene(parent,874,558));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }));
        timeline.play();
    }

    @FXML
    private void selectXML() {
        FileChooser fileChooser = new FileChooser();

        File file = fileChooser.showOpenDialog(null);
        controlador.cambiarArchivoComponente(file.getAbsolutePath());

        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("The videos of your XML File were added to AppVideo."));
        JFXDialog dialogAdded = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);

        JFXButton thanks = new JFXButton("Continue");
        thanks.setBackground(new Background(new BackgroundFill(Color.GRAY,null,null)));
        thanks.setTextFill(Color.BLACK);

        thanks.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialogAdded.close();
            }
        });
        content.setActions(thanks);
        dialogAdded.show();
    }
}
