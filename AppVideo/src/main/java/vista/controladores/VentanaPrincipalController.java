package main.java.vista.controladores;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import javafx.geometry.Insets;
import javafx.scene.CacheHint;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import main.java.controlador.AppVideo;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.java.model.CatalogoVideos;
import main.java.model.Etiqueta;
import main.java.model.ListaVideos;
import main.java.model.Video;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public class VentanaPrincipalController implements Initializable {

    @FXML
    private StackPane stackPane;

    @FXML
    private GridPane gridPane;

    @FXML
    private JFXButton botonWelcome;

    @FXML
    private JFXButton botonTendencias;

    @FXML
    private JFXButton botonListas;

    @FXML
    private JFXButton botonPerfil;

    @FXML
    private JFXButton botonAddLista;

    // Sección WELCOME

    @FXML
    private GridPane gridWelcome;

    @FXML
    private JFXTextField barraBusqueda;

    @FXML
    private ScrollPane scrollWelcome;

    @FXML
    private GridPane gridScrollWelcome;

    // Sección TENDENCIAS

    @FXML
    private GridPane gridTendencias;

    @FXML
    private ScrollPane scrollTendencias;

    @FXML
    private GridPane gridScrollTendencias;

    // Sección LISTAS

    @FXML
    private GridPane gridListas;

    @FXML
    private ScrollPane scrollListas;

    @FXML
    private GridPane gridScrollListas;

    @FXML
    private VBox vboxListas;

    @FXML
    private Label nombreLista;

    @FXML
    private JFXButton botonConfigListas;

    @FXML
    private JFXButton botonBorrar;

     // Sección CONFIGURACION de LISTAS

    @FXML
    private GridPane gridConfig;

    @FXML
    private JFXTextField busquedaConfig;

    @FXML
    private ScrollPane scrollizquierda;

    @FXML
    private ScrollPane scrollDerecha;

    @FXML
    private GridPane gridScrollizquierda;

    @FXML
    private GridPane gridScrollDerecha;

    @FXML
    private VBox vboxListasConfig;

    @FXML
    private JFXButton addVideo;

    @FXML
    private JFXButton deleteVideo;

    @FXML
    private JFXButton saveList;

    @FXML
    private Label nombreListaConfig;

    // Sección NUEVA LISTA

    @FXML
    private GridPane gridNewList;

    @FXML
    private JFXTextField busquedaNewList;

    @FXML
    private ScrollPane scrollizquierdaNewList;

    @FXML
    private ScrollPane scrollDerechaNewList;

    @FXML
    private GridPane gridScrollizquierdaNewList;

    @FXML
    private GridPane gridScrollDerechaNewList;

    @FXML
    private JFXButton addVideoNewList;

    @FXML
    private JFXButton deleteVideoNewList;

    @FXML
    private JFXButton saveNewList;

    @FXML
    private Label labelNuevaLista;

    // Resto de Atributos

    private AppVideo controlador = AppVideo.getUnicaInstancia();

    private ListaVideos listaActual;
    private String gridActual;

    private String nombreNuevaLista = "";
    private List<Video> nuevaListaAux;
    private List<Video> listaConfigAux;

    private ImageView contenedorActual;


    @Override
    public void initialize(URL location , ResourceBundle resources) {

        gridActual = "welcome";
        controlador.setVideoSeleccionado(null);

        nuevaListaAux = new ArrayList<>();
        listaConfigAux = new ArrayList<>();

        listaActual = null;

        if(controlador.getUsuarioActual().isPremium()){
            if(controlador.esCumple(controlador.getUsuarioActual())) {
                felicitarCumple();
            }
        }

        List<Video> listaWelcome = new LinkedList<>(controlador.getVideosCatalogo());
        mostrarVideosPanel(listaWelcome, gridScrollWelcome, 4);

        KeyCombination play = new KeyCodeCombination(KeyCode.ENTER);
        this.gridWelcome.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (play.match(event)) {
                    try {
                        realizarBusqueda(event, "welcome");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        this.gridConfig.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (play.match(event)) {
                    try {
                        realizarBusqueda(event , "config");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        this.gridNewList.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (play.match(event)) {
                    try {
                        realizarBusqueda(event , "nuevalista");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void cambiarGrid(){
        switch (gridActual) {
            case "welcome":
                gridWelcome.setVisible(false);
                break;
            case "tendencias":
                gridTendencias.setVisible(false);
                break;
            case "lists":
                gridListas.setVisible(false);
                break;
            case "crear lista":
                gridNewList.setVisible(false);
                break;
            case "config":
                gridConfig.setVisible(false);
                break;
        }
        listaActual = null;
    }

    @FXML
    private void goWelcome(ActionEvent event){
        if(!gridActual.equals("welcome")){
            cambiarGrid();
            gridActual = "welcome";

            List<Video> listaWelcome = new LinkedList<>(controlador.getVideosCatalogo());
            gridScrollWelcome.getChildren().clear();
            mostrarVideosPanel(listaWelcome, gridScrollWelcome, 4);

            gridWelcome.setVisible(true);
            clearBarrasBusqueda();

            labelNuevaLista.setText("");
        }
        else{
            List<Video> listaWelcome = new LinkedList<>(controlador.getVideosCatalogo());
            gridScrollWelcome.getChildren().clear();
            clearBarrasBusqueda();
            mostrarVideosPanel(listaWelcome, gridScrollWelcome, 4);
        }
    }

    @FXML
    private void goTendencias(ActionEvent event){
        if(!gridActual.equals("tendencias")){
            cambiarGrid();
            gridActual = "tendencias";

            List<Video> lista = controlador.getMasReproducidosAplicacion();
            mostrarVideosPanel(lista, gridScrollTendencias, 4);

            gridTendencias.setVisible(true);
            clearBarrasBusqueda();

            labelNuevaLista.setText("");
        }
    }

    @FXML
    private void goLists(ActionEvent event){
        if(!gridActual.equals("lists")){
            cambiarGrid();
            gridActual = "lists";
            vboxListas.getChildren().clear();
            gridListas.setVisible(true);

            nombreLista.setText("");
            nombreLista.setVisible(false);
            cargarListasUsuario();

            labelNuevaLista.setText("");
        }
        gridScrollListas.getChildren().clear();
    }

    @FXML
    private void goProfile(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java/vista/fxml/VentanaPerfil.fxml"));
        Parent parent = loader.load();
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(new Scene(parent, 874, 578));
    }

    private void goReproductor(MouseEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java/vista/fxml/VentanaReproductor.fxml"));
        Parent parent = loader.load();
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(new Scene(parent, 874, 578));
    }

    @FXML
    private void goCrearLista(ActionEvent event){
        if(!gridActual.equals("crear lista")){
            cambiarGrid();
            gridActual = "crear lista";
            gridNewList.setVisible(true);
            clearBarrasBusqueda();

            List<Video> lista = new LinkedList<>(controlador.getVideosCatalogo());
            mostrarVideosPanel(lista, gridScrollizquierdaNewList, 1);
            gridScrollDerechaNewList.getChildren().clear();

            nuevaListaAux.clear();
            crearNombreLista();
        }
    }

    @FXML
    private void goConfig(ActionEvent event){
        if(!gridActual.equals("config")){
            cambiarGrid();
            gridActual = "config";
            gridConfig.setVisible(true);

            gridScrollizquierda.getChildren().clear();
            gridScrollDerecha.getChildren().clear();
            clearBarrasBusqueda();
            cargarListasConfig();
        }
    }

    private void crearNombreLista() {
        JFXDialogLayout cuadroDialog = new JFXDialogLayout();
        cuadroDialog.setHeading(new Text("Write the new list name (not empty): "));
        JFXDialog dialog1 = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);
        JFXTextField campoNombre = new JFXTextField();

        campoNombre.setPrefWidth(100);
        campoNombre.setMaxWidth(Control.USE_PREF_SIZE);

        JFXButton aceptar = new JFXButton("Continue");
        JFXButton rechazar = new JFXButton("Discard");

        aceptar.setBackground(new Background(new BackgroundFill(Color.DARKSEAGREEN,null,null)));
        aceptar.setTextFill(Color.WHITE);
        rechazar.setBackground(new Background(new BackgroundFill(Color.INDIANRED,null,null)));
        rechazar.setTextFill(Color.WHITE);

        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nombreNuevaLista = campoNombre.getText();
                labelNuevaLista.setText(nombreNuevaLista);
                dialog1.close();
            }
        });

        rechazar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog1.close();
                goLists(event);
            }
        });

        cuadroDialog.setBody(campoNombre);
        cuadroDialog.setActions(aceptar, rechazar);
        dialog1.show();
    }

    private void felicitarCumple(){
        JFXDialogLayout cuadroDialog = new JFXDialogLayout();
        cuadroDialog.setHeading(new Text("Happy birthday " + controlador.getUsuarioActual().getNombre() + "\r   \n" +
                " Have a nice day with our videos, and thank you for making AppVideo possible."));
        JFXDialog dialog = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);
        JFXButton gracias = new JFXButton("Thank you");

        gracias.setBackground(new Background(new BackgroundFill(Color.CORAL,null,null)));
        gracias.setTextFill(Color.WHITE);

        gracias.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        cuadroDialog.setActions(gracias);
        dialog.show();
    }

    private void realizarBusqueda(KeyEvent event , String panel) {

        List<Video> listaBusqueda = new LinkedList<>();
        List<Video> listaCatalogo = new LinkedList<>(controlador.getVideosCatalogo());
        String palabraBusqueda = "";

        switch (panel) {
            case "welcome":
                palabraBusqueda = barraBusqueda.getText();
                break;
            case "config":
                palabraBusqueda = busquedaConfig.getText();
                break;
            case "nuevalista":
                palabraBusqueda = busquedaNewList.getText();
                break;
        }

        for(Video video : listaCatalogo) {

            if(video.getTitulo().toLowerCase().contains(palabraBusqueda.toLowerCase())){
                listaBusqueda.add(video);
            }

            for(Etiqueta e : video.getEtiquetas()){

                if(e.getNombre().toLowerCase().contains(palabraBusqueda.toLowerCase())){
                    if(!listaBusqueda.contains(video)){
                        listaBusqueda.add(video);
                    }
                }
            }

            if(controlador.getUsuarioActual().isPremium()){
                if(controlador.getUsuarioActual().getTipoFiltro().getValor().toLowerCase().equals(video.getGenero().toLowerCase())){
                    listaBusqueda.remove(video);
                }
            }
        }


        switch (panel) {
            case "welcome":
                gridScrollWelcome.getChildren().clear();
                mostrarVideosPanel(listaBusqueda, gridScrollWelcome, 4);
                break;
            case "config":
                gridScrollizquierda.getChildren().clear();
                mostrarVideosPanel(listaBusqueda, gridScrollizquierda, 1);
                break;
            case "nuevalista":
                gridScrollizquierdaNewList.getChildren().clear();
                mostrarVideosPanel(listaBusqueda, gridScrollizquierdaNewList, 1);
                break;
        }
    }

    private void clearBarrasBusqueda() {
        barraBusqueda.setText("");
        busquedaConfig.setText("");
        busquedaNewList.setText("");
    }

    private void cargarListasConfig(){

        vboxListasConfig.getChildren().clear();

        for (ListaVideos lista : controlador.recuperarListasUsuario(controlador.getUsuarioActual())){
            JFXButton boton = new JFXButton();
            boton.setPrefWidth(203);
            boton.setPrefHeight(27);
            boton.setText(lista.getNombreLista());
            boton.setStyle("-fx-text-fill: white");
            vboxListasConfig.getChildren().add(boton);

            boton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    List<Video> catalogoVideos = new LinkedList<>(controlador.getVideosCatalogo());
                    mostrarVideosPanel(catalogoVideos, gridScrollizquierda, 1);

                    listaConfigAux.clear();
                    listaConfigAux.addAll(lista.getVideos());
                    gridScrollDerecha.getChildren().clear();
                    mostrarVideosPanel(listaConfigAux, gridScrollDerecha, 1);

                    nombreListaConfig.setText(lista.getNombreLista());
                    nombreListaConfig.setVisible(true);
                    listaActual = lista;
                }
            });
        }
    }

    private void cargarListasUsuario() {

        for (ListaVideos lista : controlador.recuperarListasUsuario(controlador.getUsuarioActual())){
            JFXButton boton = new JFXButton();
            boton.setPrefWidth(203);
            boton.setPrefHeight(27);
            boton.setText(lista.getNombreLista());
            boton.setStyle("-fx-text-fill: white");
            vboxListas.getChildren().add(boton);

            boton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    gridScrollListas.getChildren().clear();

                    mostrarVideosPanel(lista.getVideos(), gridScrollListas, 3);
                    nombreLista.setText(lista.getNombreLista());
                    nombreLista.setVisible(true);
                    listaActual = lista;
                }
            });
        }

        JFXButton botonRecientes = new JFXButton();
        botonRecientes.setPrefWidth(203);
        botonRecientes.setPrefHeight(27);
        botonRecientes.setText("Recent");
        botonRecientes.setStyle("-fx-text-fill: white");
        vboxListas.getChildren().add(botonRecientes);

        botonRecientes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                nombreLista.setText("Recent");
                nombreLista.setVisible(true);
                gridScrollListas.getChildren().clear();
                mostrarVideosPanel(controlador.getVideosRecientes(controlador.getUsuarioActual()),
                        gridScrollListas, 3);
            }
        });

        JFXButton botonMasvistos = new JFXButton();
        botonMasvistos.setPrefWidth(203);
        botonMasvistos.setPrefHeight(27);
        botonMasvistos.setText("Most watched");
        botonMasvistos.setStyle("-fx-text-fill: white");
        vboxListas.getChildren().add(botonMasvistos);

        botonMasvistos.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                nombreLista.setText("Most watched");
                nombreLista.setVisible(true);
                gridScrollListas.getChildren().clear();

                mostrarVideosPanel(controlador.getMasReproducidosUsuario(controlador.getUsuarioActual()),
                        gridScrollListas, 3);
            }
        });
    }

    @FXML
    private void borrarLista(ActionEvent event) {

        if(listaActual != null) {
            controlador.borrarListaVideos(controlador.getUsuarioActual(), listaActual);

            JFXDialogLayout cuadroDialog = new JFXDialogLayout();
            cuadroDialog.setHeading(new Text("List deleted. It's time to create a better one?"));
            JFXDialog dialog1 = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

            JFXButton aceptar = new JFXButton("Continue");
            aceptar.setBackground(new Background(new BackgroundFill(Color.YELLOWGREEN,null,null)));
            aceptar.setTextFill(Color.BLACK);

            aceptar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog1.close();
                    goWelcome(event);
                }
            });

            cuadroDialog.setActions(aceptar);
            dialog1.show();
        }
        else{
            JFXDialogLayout cuadroDialog = new JFXDialogLayout();
            cuadroDialog.setHeading(new Text("Hey! You can't delete the air. Select a list first..."));
            JFXDialog dialog1 = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

            JFXButton aceptar = new JFXButton("Okay");
            aceptar.setBackground(new Background(new BackgroundFill(Color.DARKORANGE,null,null)));
            aceptar.setTextFill(Color.WHITE);

            aceptar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog1.close();
                }
            });
            cuadroDialog.setActions(aceptar);
            dialog1.show();
        }
    }

    @FXML
    private void addVideoNuevaLista(ActionEvent event){
        nuevaListaAux.add(controlador.getVideoSeleccionado());
        gridScrollDerechaNewList.getChildren().clear();
        mostrarVideosPanel(nuevaListaAux, gridScrollDerechaNewList, 1);
        deseleccionar();
    }

    @FXML
    private void addVideoListaConfig(ActionEvent event){
        listaConfigAux.add(controlador.getVideoSeleccionado());
        gridScrollDerecha.getChildren().clear();
        mostrarVideosPanel(listaConfigAux, gridScrollDerecha, 1);
        deseleccionar();
    }

    @FXML
    private void deleteVideoNuevaLista(ActionEvent event){
        nuevaListaAux.remove(controlador.getVideoSeleccionado());
        gridScrollDerechaNewList.getChildren().clear();
        mostrarVideosPanel(nuevaListaAux, gridScrollDerechaNewList, 1);
        deseleccionar();
    }

    @FXML
    private void deleteVideoListaConfig(ActionEvent event){
        listaConfigAux.remove(controlador.getVideoSeleccionado());
        gridScrollDerecha.getChildren().clear();
        mostrarVideosPanel(listaConfigAux, gridScrollDerecha, 1);
        deseleccionar();
    }

    private void mostrarVideosPanel(List<Video> videos, GridPane gridPane, int numColumnas) {

            gridPane.setPadding(new Insets(10,10,10,10));
            gridPane.setVgap(10);
            gridPane.setHgap(10);
            int columna = 0;
            int fila = 0;

            for(Video video : videos) {

                String url = video.getUrl();

                String idvideo = url.substring(url.indexOf(61) + 1);

                Image imagenFX = new Image("http://img.youtube.com/vi/" + idvideo + "/0.jpg", true);

                ImageView contenedor = new ImageView(imagenFX);

                contenedor.setFitWidth(160);
                contenedor.setFitHeight(90);
                contenedor.setPreserveRatio(true);

                contenedor.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {

                        if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){

                            if((mouseEvent.getClickCount() == 1) && (!gridPane.getId().equals("gridScrollWelcome")) &&
                                    (!gridPane.getId().equals("gridScrollTendencias") && (!gridPane.getId().equals("gridListas")))){

                                if(controlador.getVideoSeleccionado()!=null){
                                    deseleccionar();
                                }
                                controlador.setVideoSeleccionado(video);

                                ColorAdjust blackout = new ColorAdjust();
                                blackout.setBrightness(0.3);

                                contenedor.setEffect(blackout);
                                contenedor.setCache(true);
                                contenedor.setCacheHint(CacheHint.SPEED);

                                contenedorActual = contenedor;
                            }
                            if(mouseEvent.getClickCount()==2){
                                try {
                                    controlador.setVideoSeleccionado(video);
                                    controlador.addReproduccion(video);
                                    controlador.addVideoVistoUsuario();
                                    controlador.addVideoRecientes(controlador.getUsuarioActual(), video);
                                    controlador.addVideoMasVistos(controlador.getUsuarioActual(), video);
                                    goReproductor(mouseEvent);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });

                gridPane.add(contenedor, columna, fila);

                if(numColumnas != 0){
                    if(columna == numColumnas){
                        columna = 0;
                        fila++;
                    }
                    else{
                        columna++;
                    }
                }
                else {
                    fila++;
                }
            }
    }

    private void deseleccionar() {
        ColorAdjust blackout = new ColorAdjust();
        blackout.setBrightness(0.0);
        contenedorActual.setEffect(blackout);
    }

    @FXML
    private void crearNuevaLista(ActionEvent event) {

        controlador.crearLista(controlador.getUsuarioActual(), nombreNuevaLista, nuevaListaAux);

        JFXDialogLayout cuadroDialog = new JFXDialogLayout();
        cuadroDialog.setHeading(new Text("List created! You will find this new list the next time at Lists section."));
        JFXDialog dialog1 = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

        JFXButton aceptar = new JFXButton("Continue");
        aceptar.setBackground(new Background(new BackgroundFill(Color.CHOCOLATE,null,null)));
        aceptar.setTextFill(Color.WHITE);

        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog1.close();
                goLists(event);
            }
        });

        cuadroDialog.setActions(aceptar);
        dialog1.show();
    }

    @FXML
    private void guardarListaConfig(ActionEvent event) {
        controlador.actualizarLista(controlador.getUsuarioActual(), nombreListaConfig.getText(), listaConfigAux);

        JFXDialogLayout cuadroDialog = new JFXDialogLayout();
        cuadroDialog.setHeading(new Text("List edited! Everyone makes mistakes..."));
        JFXDialog dialog1 = new JFXDialog(stackPane, cuadroDialog, JFXDialog.DialogTransition.CENTER);

        JFXButton aceptar = new JFXButton("Continue");
        aceptar.setBackground(new Background(new BackgroundFill(Color.CHOCOLATE,null,null)));
        aceptar.setTextFill(Color.WHITE);

        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog1.close();
                goLists(event);
            }
        });
        cuadroDialog.setActions(aceptar);
        dialog1.show();
    }
















}
