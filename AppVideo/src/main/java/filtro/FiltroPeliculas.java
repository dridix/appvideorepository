package main.java.filtro;

public class FiltroPeliculas implements IFiltro{

    private static final long serialVersionUID = 1L;
    private String valor;

    public FiltroPeliculas() {
        this.valor = "Peliculas";
    }

    @Override
    public String getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "Peliculas";
    }
}
