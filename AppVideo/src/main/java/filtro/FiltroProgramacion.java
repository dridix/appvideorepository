package main.java.filtro;

public class FiltroProgramacion implements IFiltro{

    private static final long serialVersionUID = 1L;
    private String valor;

    public FiltroProgramacion() {
        this.valor = "Programacion";
    }

    @Override
    public String getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "Programacion";
    }
}
