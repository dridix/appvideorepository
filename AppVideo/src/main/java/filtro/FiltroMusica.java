package main.java.filtro;

public class FiltroMusica implements IFiltro{

    private static final long serialVersionUID = 1L;
    private String valor;

    public FiltroMusica() {
        this.valor = "Musica";
    }

    @Override
    public String getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "Musica";
    }
}
