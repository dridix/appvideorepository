package main.java.filtro;

public class FiltroDeporte implements IFiltro{

    private static final long serialVersionUID = 1L;
    private String valor;

    public FiltroDeporte() {
        this.valor = "Deporte";
    }

    @Override
    public String getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "Deporte";
    }
}
