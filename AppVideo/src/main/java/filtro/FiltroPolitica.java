package main.java.filtro;

public class FiltroPolitica implements IFiltro{

    private static final long serialVersionUID = 1L;
    private String valor;

    public FiltroPolitica() {
        this.valor = "Politica";
    }

    @Override
    public String getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "Politica";
    }
}
