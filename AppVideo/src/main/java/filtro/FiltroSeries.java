package main.java.filtro;

public class FiltroSeries implements IFiltro{

    private static final long serialVersionUID = 1L;
    private String valor;

    public FiltroSeries() {
        this.valor = "Series";
    }

    @Override
    public String getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "Series";
    }
}
