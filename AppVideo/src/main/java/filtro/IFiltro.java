package main.java.filtro;

import java.io.Serializable;

public interface IFiltro extends Serializable {

     long serialVersionUID = 1L;
     String getValor();
}
