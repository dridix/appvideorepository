package main.java.launcher;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Launcher extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java/vista/fxml/VentanaLogin.fxml"));

        Parent root = loader.load();
        Scene scene = new Scene(root);
        primaryStage.setMinWidth(874);
        primaryStage.setMinHeight(578);
        primaryStage.setMaxWidth(874);
        primaryStage.setMaxHeight(578);
        primaryStage.setResizable(true);
        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
