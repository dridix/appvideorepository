package main.java.model;

import main.java.dao.AbsFactoriaDAO;
import main.java.dao.ExceptionDAO;
import main.java.dao.FactoriaDAO;

import java.util.*;
import java.util.stream.Collectors;

public class CatalogoVideos {

    private Map<String, Video> videos;
    private Set<String> generos;

    private static CatalogoVideos unicaInstancia;
    private AbsFactoriaDAO factoriaDAO;

    private CatalogoVideos() {
        this.generos = new HashSet<>();
        this.generos.add("");
        videos = new HashMap<>();

        try {
            factoriaDAO = FactoriaDAO.getInstancia(FactoriaDAO.FACTORIA_DAO);
            List<Video> videos = factoriaDAO.getVideoDAO().recuperarTodosVideos();
            for (Video v : videos) {
                this.videos.put(v.getTitulo(), v);
            }
        } catch (ExceptionDAO e) {
            e.printStackTrace();
        }
        for (Video v : this.videos.values()) {
            addGenero(v.getGenero());
        }
    }

    public static CatalogoVideos getUnicaInstancia() {
        if(unicaInstancia == null) {
            unicaInstancia = new CatalogoVideos();
        }
        return unicaInstancia;
    }

    public void addGenero(String genero) {
        this.generos.add(genero.toUpperCase());
    }

    public void addVideo(Video video) {
        videos.put(video.getTitulo(),video);
        if(!this.generos.contains(video.getGenero())){
            addGenero(video.getGenero());
        }
    }

    public boolean contieneVideo(Video video) {
        return this.videos.containsValue(video);
    }

    public List<Video> getVideos() {
        return new ArrayList<>(this.videos.values());
    }

    public List<Video> getMasReproducidos(){
        Comparator<Video> byNRep = (Video o1, Video o2)->Integer.compare(o2.getNumReproducciones(),o1.getNumReproducciones());
        ArrayList<Video> aux = new ArrayList<>((getVideos()));
        aux.sort(byNRep);
        return aux.stream().limit(10).collect(Collectors.toList());
    }

}
