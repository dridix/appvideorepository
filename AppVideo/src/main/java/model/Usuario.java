package main.java.model;

import main.java.filtro.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

@Entity
@Table(name = "Usuario")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name = "nombreUsuario")
	private String nombreUsuario;

	@Column(name = "password")
	private String password;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "apellidos")
	private String apellidos;

	@Column(name = "email")
	private String email;

	@Column(name = "fechaNacimiento")
	private LocalDate fechaNacimiento;

	@Column(name = "premium")
	private boolean premium;

	@Column(name = "VideosVistos")
	private int numVideosVistos;

	@Column(name = "fechaRegistro")
	private LocalDate fechaRegistro;

	@Column(name = "FiltroAplicado")
	private IFiltro tipoFiltro;

	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch = FetchType.EAGER)
	private List<ListaVideos> listasReproduccion;

	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USUARIO_RECIENTES")
	private List<Video> listaRecientes;

	@Fetch(FetchMode.SELECT)
	@ElementCollection(fetch = FetchType.EAGER)
	private Map<Video, Integer> mapaMasVistos;


	public Usuario(String nombre , String apellidos , String correo , String username , String password,
				   LocalDate fechaNacimiento){
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.email = correo;
		this.nombreUsuario = username;
		this.password = password;
		this.fechaNacimiento = fechaNacimiento;
		this.premium = false;
		this.numVideosVistos = 0;

		this.listasReproduccion = new ArrayList<>();
		this.listaRecientes = new ArrayList<>();
		this.mapaMasVistos = new HashMap<>();

		setTipoFiltro("NoFiltro");
	}

	public Usuario() {

	}

	// Métodos getters y setters necesarios.

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public boolean isPremium() {
		return premium;
	}

	public void setPremium(boolean premium) {
		this.premium = premium;
	}

	public IFiltro getTipoFiltro() {
		return tipoFiltro;
	}

	public void setTipoFiltro(String tipoFiltro) {
		switch (tipoFiltro){
			case "NoFiltro":
				this.tipoFiltro = new NoFiltro();
				break;

			case "Politica":
				this.tipoFiltro = new FiltroPolitica();
				break;

			case "Musica":
				this.tipoFiltro = new FiltroMusica();
				break;

			case "Deporte":
				this.tipoFiltro = new FiltroDeporte();
				break;

			case "Series":
				this.tipoFiltro = new FiltroSeries();
				break;

			case "Programacion":
				this.tipoFiltro = new FiltroProgramacion();
				break;

			case "Peliculas":
				this.tipoFiltro = new FiltroPeliculas();
				break;
		}
	}

	public List<ListaVideos> getListasReproduccion() {
		return this.listasReproduccion;
	}

	public LocalDate getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(LocalDate fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public int getNumVideosVistos() {
		return numVideosVistos;
	}

	public void addVideoVisto() {
		this.numVideosVistos++;
	}

	public List<Video> getListaRecientes() {
		return this.listaRecientes;
	}

	public void addVideoMasVistos(Video video) {
		if(mapaMasVistos.containsKey(video)){
			int x = mapaMasVistos.get(video);
			mapaMasVistos.put(video, x+1);
		}
		else{
			mapaMasVistos.put(video, 1);
		}
	}

	public Map<Video, Integer> getMapaMasVistos() {
		return mapaMasVistos;
	}

	// Funcionalidad de la clase

	public void addListaVideos(ListaVideos nuevaLista){
		this.listasReproduccion.add(nuevaLista);
	}

	public void addRecientes(Video video) {
		if(!this.listaRecientes.contains(video)) {
			if(this.listaRecientes.size() >= 5) {
				this.listaRecientes.remove(4);
				this.listaRecientes.add(0, video);
			}else {
				this.listaRecientes.add(0,video);
			}
		}
	}

	public ListaVideos getListaPorNombre(String nombreLista) {
		for (ListaVideos lista : this.listasReproduccion) {
			if(lista.getNombreLista().equals(nombreLista)) {
				return lista;
			}
		}
		return null;
	}

	public void deleteLista(ListaVideos lista) {
		this.listasReproduccion.remove(lista);
	}

	public ListaVideos newLista(String nombreLista, List<Video> videos) {
		return new ListaVideos(nombreLista,videos);
	}

	@Override
	public String toString() {
		return "Usuario [nombre=" + nombre + ", apellidos=" + apellidos + ", email=" + email
				+ ", nombreUsuario=" + nombreUsuario + ", password=" + password + ", fechaNacimiento=" + fechaNacimiento
				+ ", premium=" + premium +"]";
	}

}
