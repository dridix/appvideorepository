package main.java.model;

import main.java.dao.AbsFactoriaDAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CatalogoUsuarios {

    private Map<String, Usuario> nicksUsuarios;
    private Map<Integer,Usuario> idUsuarios;
    private static CatalogoUsuarios unicaInstancia;

    private AbsFactoriaDAO absFactoriaDAO;

    private CatalogoUsuarios() {
        nicksUsuarios = new HashMap<>();
        idUsuarios = new HashMap<>();

        absFactoriaDAO = AbsFactoriaDAO.getInstancia();

        List<Usuario> usuarios = (List<Usuario>) absFactoriaDAO.getUsuarioDAO().recuperarTodosUsuarios();
        for (Usuario usuario : usuarios) {
            this.nicksUsuarios.put(usuario.getNombreUsuario(), usuario);
            this.idUsuarios.put(usuario.getId(), usuario);
        }
    }

    public static CatalogoUsuarios getUnicaInstancia() {
        if(unicaInstancia == null) {
            unicaInstancia = new CatalogoUsuarios();
        }
        return unicaInstancia;
    }

    public Usuario getUsuario(int codigo) {
        return this.idUsuarios.get(codigo);
    }

    public Usuario getUsuario(String username) {
        return this.nicksUsuarios.get(username);
    }

    public void creaUsuario(Usuario usuario) {
        System.out.println("Usuario añadido: " + usuario.toString());
        this.nicksUsuarios.put(usuario.getNombreUsuario(),usuario);
        this.idUsuarios.put(usuario.getId(),usuario);
    }

}
