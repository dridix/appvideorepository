package main.java.model;

import main.java.dao.AbsFactoriaDAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CatalogoEtiquetas {

    private Map<String, Etiqueta> textosEtiquetas;
    private Map<Integer, Etiqueta> idEtiquetas;
    private static CatalogoEtiquetas unicaInstancia;

    private AbsFactoriaDAO absFactoriaDAO;

    private CatalogoEtiquetas() {
        textosEtiquetas = new HashMap<>();
        idEtiquetas = new HashMap<>();

        absFactoriaDAO = AbsFactoriaDAO.getInstancia();

        List<Etiqueta> etiquetas = (List<Etiqueta>) absFactoriaDAO.getEtiquetaDAO().recuperarTodasEtiquetas();
        for(Etiqueta e : etiquetas) {
            this.textosEtiquetas.put(e.getNombre(), e);
            this.idEtiquetas.put(e.getId(), e);
        }
    }

    public static CatalogoEtiquetas getUnicaInstancia() {
        if(unicaInstancia == null) {
            unicaInstancia = new CatalogoEtiquetas();
        }
        return unicaInstancia;
    }

    public Etiqueta getEtiqueta(String texto) {
        return this.textosEtiquetas.get(texto);
    }

    public boolean existeEtiqueta(String texto) {
        return this.textosEtiquetas.containsKey(texto);
    }

    public void creaEtiqueta(Etiqueta etiqueta) {
        System.out.println("Etiqueta añadida: " + etiqueta.toString());
        this.textosEtiquetas.put(etiqueta.getNombre(),etiqueta);
        this.idEtiquetas.put(etiqueta.getId(),etiqueta);
    }

    public void borraEtiqueta(Etiqueta etiqueta) {
        this.textosEtiquetas.remove(etiqueta.getNombre());
        this.idEtiquetas.remove(etiqueta.getId());
    }

}
