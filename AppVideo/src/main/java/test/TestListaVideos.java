package main.java.test;

import main.java.model.ListaVideos;
import main.java.model.Video;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestListaVideos {

    static ListaVideos listaVideos;

    @BeforeClass
    public static void before() {
        listaVideos = new ListaVideos("nombreLista", new LinkedList<>());
    }

    @Test
    public void testGet() {
        assertEquals("test getNombreLista","nombreLista",listaVideos.getNombreLista());
        assertEquals("test getVideos",new LinkedList<Video>(),listaVideos.getVideos());
    }

    @Test
    public void testSet() {

        ListaVideos listaTest = new ListaVideos(listaVideos.getNombreLista(),listaVideos.getVideos());

        listaVideos.setNombreLista("otroNombreLista");
        LinkedList<Video> aux = new LinkedList<>();
        aux.add(new Video());
        listaVideos.setVideos(aux);

        assertNotEquals("test setNombre",listaTest.getNombreLista(),listaVideos.getNombreLista());
        assertNotEquals("test setCanciones",listaTest.getVideos(),listaVideos.getVideos());

    }











}
