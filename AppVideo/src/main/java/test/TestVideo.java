package main.java.test;

import main.java.model.Video;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestVideo {

    static Video video;

    @BeforeClass
    public static void before() {
        video = new Video("titulo","url");
    }

    @Test
    public void testGet() {
        assertEquals("test getTitulo","titulo",video.getTitulo());
        assertEquals("test getUrl","url",video.getUrl());
    }

    @Test
    public void testSet() {

        Video videoTest = new Video(video.getTitulo(),video.getUrl());

        video.setTitulo("nuevoTitulo");
        video.setUrl("nuevaUrl");

        assertNotEquals("test setTitulo",videoTest.getTitulo(),video.getTitulo());
        assertNotEquals("test setUrl",videoTest.getUrl(),video.getUrl());
    }

}
