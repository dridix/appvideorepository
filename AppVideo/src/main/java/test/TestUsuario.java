package main.java.test;

import main.java.model.Usuario;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestUsuario {

    static Usuario usuario;

    @BeforeClass
    public static void before() {
        usuario = new Usuario("nombre" , "apellidos" , "correo" , "username" , "password" , LocalDate.now());
    }

    @Test
    public void testGet() {
        assertEquals("test getNombre" , "nombre" , usuario.getNombre());
        assertEquals("test getApellidos" , "apellidos" , usuario.getApellidos());
        assertEquals("test getEmail" , "email" , usuario.getEmail());
        assertEquals("test getNick" , "nick" , usuario.getNombreUsuario());
        assertEquals("test getPassword" , "password" , usuario.getPassword());
        assertEquals("test getFechaNacimiento" , LocalDate.now() , usuario.getFechaNacimiento());
    }

    @Test
    public void testSet() {

        Usuario utest = new Usuario(usuario.getNombre() , usuario.getApellidos() , usuario.getEmail() ,
                usuario.getNombreUsuario() , usuario.getPassword() , usuario.getFechaNacimiento());

        usuario.setNombre("otroNombre");
        usuario.setApellidos("otrosApellidos");
        usuario.setEmail("otroEmail");
        usuario.setNombreUsuario("otroNick");
        usuario.setPassword("otraPassword");
        usuario.setFechaNacimiento(LocalDate.of(1995 , 5 , 12));

        assertNotEquals("test setNombre" , utest.getNombre() , usuario.getNombre());
        assertNotEquals("test setApellidos" , utest.getApellidos() , usuario.getApellidos());
        assertNotEquals("test setEmail" , utest.getEmail() , usuario.getEmail());
        assertNotEquals("test setNick" , utest.getNombreUsuario() , usuario.getNombreUsuario());
        assertNotEquals("test setPassword" , utest.getPassword() , usuario.getPassword());
        assertNotEquals("test setFechaNacimiento" , utest.getFechaNacimiento() , usuario.getFechaNacimiento());
    }
}
