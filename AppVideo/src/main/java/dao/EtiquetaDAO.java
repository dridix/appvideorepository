package main.java.dao;

import main.java.model.Etiqueta;
import org.hibernate.Session;
import org.hibernate.Transaction;
import main.java.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class EtiquetaDAO implements IntEtiquetaDAO{

    @Override
    public void registrarEtiqueta(Etiqueta etiqueta) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.save(etiqueta);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (transaction != null)
                transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void borrarEtiqueta(Etiqueta etiqueta) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.delete(etiqueta);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (transaction != null)
                transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void modificarEtiqueta(Etiqueta etiqueta) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.update(etiqueta);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (transaction != null)
                transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public Etiqueta recuperarEtiqueta(int id) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Etiqueta etiqueta = session.get(Etiqueta.class, id);
            session.getTransaction().commit();
            return etiqueta;
        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public List<Etiqueta> recuperarTodasEtiquetas() {

        List<Etiqueta> listaEtiquetasRecuperadas = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            listaEtiquetasRecuperadas = (ArrayList<Etiqueta>) session.createQuery("FROM Etiqueta").list();
            session.getTransaction().commit();
            return listaEtiquetasRecuperadas;
        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }
}
