package main.java.dao;

public class FactoriaDAO extends AbsFactoriaDAO {

    public FactoriaDAO() {
    }

    @Override
    public IntUsuarioDAO getUsuarioDAO() {
        return new UsuarioDAO();
    }

    @Override
    public IntVideoDAO getVideoDAO() {
        return new VideoDAO();
    }

    @Override
    public IntListaVideosDAO getListaVideosDAO() {
        return new ListaVideosDAO();
    }

    @Override
    public IntEtiquetaDAO getEtiquetaDAO(){
        return new EtiquetaDAO();
    }
}
