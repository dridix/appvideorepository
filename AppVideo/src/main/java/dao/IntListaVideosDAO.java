package main.java.dao;

import main.java.model.ListaVideos;

import java.util.List;

public interface IntListaVideosDAO {

    void registrarListaVideos(ListaVideos listaVideos);
    void borrarListaVideos(ListaVideos listaVideos);
    void modificarListaVideos(ListaVideos listaVideos);
    ListaVideos recuperarListaVideos(int id);
    List<ListaVideos> recuperarTodasListaVideos();
}
