package main.java.dao;

import main.java.model.ListaVideos;
import org.hibernate.Session;
import org.hibernate.Transaction;
import main.java.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ListaVideosDAO implements IntListaVideosDAO {

    @Override
    public void registrarListaVideos(ListaVideos listaVideos) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.save(listaVideos);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (transaction != null)
                transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void borrarListaVideos(ListaVideos listaVideos) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.delete(listaVideos);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (transaction != null)
                transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void modificarListaVideos(ListaVideos listaVideos) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.update(listaVideos);
            session.getTransaction().commit();

        } catch (RuntimeException e) {
            if (transaction != null)
                transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public ListaVideos recuperarListaVideos(int id) {

        List<ListaVideos> lista = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            lista = (ArrayList<ListaVideos>) session.createQuery("from ListaVideos").list();
            session.getTransaction().commit();
            return (ListaVideos) lista;
        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ListaVideos> recuperarTodasListaVideos() {

        List<ListaVideos> listaRecuperadas = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            listaRecuperadas = (ArrayList<ListaVideos>) session.createQuery("FROM ListaVideos").list();
            session.getTransaction().commit();
            return listaRecuperadas;
        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }
}
