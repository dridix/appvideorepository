package main.java.dao;

public abstract class AbsFactoriaDAO {

    private static AbsFactoriaDAO unicaInstancia;
    public static final String FACTORIA_DAO = "main.java.dao.FactoriaDAO";

    protected AbsFactoriaDAO() {
    }

    public static AbsFactoriaDAO getInstancia(String tipo) throws ExceptionDAO {
        if (unicaInstancia == null) {
            try {
                unicaInstancia = (FactoriaDAO) Class.forName(tipo).newInstance();
            } catch (Exception e) {
                throw new ExceptionDAO(e.getMessage());
            }
        }
        return unicaInstancia;
    }

    public static AbsFactoriaDAO getInstancia(){
        try {
            return getInstancia(AbsFactoriaDAO.FACTORIA_DAO);
        } catch (ExceptionDAO e) {
            e.printStackTrace();
        }
        return null;
    }

    public abstract IntUsuarioDAO getUsuarioDAO();
    public abstract IntVideoDAO getVideoDAO();
    public abstract IntListaVideosDAO getListaVideosDAO();
    public abstract IntEtiquetaDAO getEtiquetaDAO();

}
