package main.java.dao;

import main.java.model.Etiqueta;

import java.util.List;

public interface IntEtiquetaDAO {

    void registrarEtiqueta(Etiqueta etiqueta);
    void borrarEtiqueta(Etiqueta etiqueta);
    void modificarEtiqueta(Etiqueta etiqueta);
    Etiqueta recuperarEtiqueta(int id);
    List<Etiqueta> recuperarTodasEtiquetas();
}
