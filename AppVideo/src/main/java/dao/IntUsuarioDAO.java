package main.java.dao;

import main.java.model.Usuario;

import java.util.List;

public interface IntUsuarioDAO {

    void registrarUsuario(Usuario usuario);
    void borrarUsuario(Usuario usuario);
    void modificarUsuario(Usuario usuario);
    Usuario recuperarUsuario(int id);
    List<Usuario> recuperarTodosUsuarios();
}
