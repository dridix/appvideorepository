package main.java.dao;

import main.java.model.Video;
import java.util.List;

public interface IntVideoDAO {

    void registrarVideo(Video video);
    void borrarVideo(Video video);
    void modificarVideo(Video video);
    Video recuperarVideo(int id);
    List<Video> recuperarTodosVideos();
}
