package main.java.controlador;

import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import main.java.dao.*;
import main.java.model.*;
import com.itextpdf.text.*;
import umu.tds.videos.BuscadorVideos;
import umu.tds.videos.VideosEvent;
import umu.tds.videos.VideosListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import static java.util.stream.Collectors.toMap;

public class AppVideo {

    private static AppVideo unicaInstancia;

    private AbsFactoriaDAO factoriaDAO;
    private IntUsuarioDAO usuarioDAO;
    private IntListaVideosDAO listaVideosDAO;
    private IntVideoDAO videoDAO;
    private IntEtiquetaDAO etiquetaDAO;
    private int videosXMLAdded;
    private BuscadorVideos buscadorVideos;

    Usuario usuarioActual;
    private Video videoSeleccionado;

    private AppVideo(){

        videosXMLAdded = 0;
        usuarioActual = null;
        factoriaDAO = AbsFactoriaDAO.getInstancia();
        listaVideosDAO = factoriaDAO.getListaVideosDAO();
        usuarioDAO = factoriaDAO.getUsuarioDAO();
        videoDAO = factoriaDAO.getVideoDAO();
        etiquetaDAO = factoriaDAO.getEtiquetaDAO();

        buscadorVideos = new BuscadorVideos();

        CatalogoVideos.getUnicaInstancia();
        CatalogoUsuarios.getUnicaInstancia();
        CatalogoEtiquetas.getUnicaInstancia();

        buscadorVideos.addVideosListener(new VideosListener() {

            @Override
            public void nuevosVideos(VideosEvent evento) {
                videosXMLAdded = 0;
                for (umu.tds.videos.Video video : evento.getVideos().getVideo()) {
                    Video v = new Video(video.getTitulo(), video.getUrl());

                    List<Etiqueta> etiquetasAux = new LinkedList<>();
                    for(umu.tds.videos.Etiqueta e : video.getEtiqueta()){

                        if(!CatalogoEtiquetas.getUnicaInstancia().existeEtiqueta(e.getNombre())){
                            Etiqueta etiquetaModelo = new Etiqueta(e.getNombre());
                            etiquetasAux.add(etiquetaModelo);
                            etiquetaDAO.registrarEtiqueta(etiquetaModelo);
                            CatalogoEtiquetas.getUnicaInstancia().creaEtiqueta(etiquetaModelo);
                        }
                        else{
                            etiquetasAux.add(CatalogoEtiquetas.getUnicaInstancia().getEtiqueta(e.getNombre()));
                        }
                    }
                    v.setEtiquetas(etiquetasAux);
                    v.setGenero(video.getEtiqueta().get(0).getNombre());

                    if(!CatalogoVideos.getUnicaInstancia().contieneVideo(v)) {
                        videoDAO.registrarVideo(v);
                        CatalogoVideos.getUnicaInstancia().addVideo(v);
                        videosXMLAdded++;
                    }
                }
            }
        });
    }

    public static AppVideo getUnicaInstancia() {
        if ( unicaInstancia == null) {
            unicaInstancia = new AppVideo();
        }
        return unicaInstancia;
    }

    public boolean existeUsuario(String username) {
        return CatalogoUsuarios.getUnicaInstancia().getUsuario(username) != null;
    }

    public boolean login(String username, String password) {
        Usuario user = CatalogoUsuarios.getUnicaInstancia().getUsuario(username);
        if (user != null && user.getPassword().equals(password)) {
            usuarioActual = user;
            return true;
        }
        return false;
    }

    public void logout() {
        this.usuarioActual = null;
    }

    public boolean esCumple(Usuario usuario) {
        LocalDate hoy = LocalDate.now();
        LocalDate nacimiento = usuario.getFechaNacimiento();
        return (hoy.getDayOfYear() == nacimiento.getDayOfYear());
    }

    public void generarPDF(Usuario usuario , String ruta) {

        Document pdf = new Document(PageSize.A4, 35, 30, 50, 50);
        FileOutputStream ficheroSalida = null;
        Font fuenteTitulo = FontFactory.getFont(FontFactory.HELVETICA, 20);
        Font fuenteLista = FontFactory.getFont(FontFactory.HELVETICA, 14);
        fuenteLista.setColor(BaseColor.ORANGE);
        try {
            ficheroSalida = new FileOutputStream(ruta + "/listas"+usuario.getNombreUsuario()+".pdf");
            PdfWriter.getInstance(pdf, ficheroSalida);
            pdf.open();

            Paragraph p1 = new Paragraph();
            p1.add(new Phrase("Listas de reproducción de AppVideo del usuario "+usuario.getNombre()+" "+usuario.getApellidos(), fuenteTitulo));
            p1.add(new Phrase(Chunk.NEWLINE));
            p1.add(new Phrase(Chunk.NEWLINE));
            p1.add(new Phrase("Nick : "+ usuario.getNombreUsuario(), fuenteTitulo));
            p1.add(new Phrase(Chunk.NEWLINE));
            p1.add(new Phrase(Chunk.NEWLINE));
            pdf.add(p1);
            for (ListaVideos lista : usuario.getListasReproduccion()) {
                PdfPTable tabla = new PdfPTable(3);
                Paragraph p2 = new Paragraph();
                p2.add(new Phrase("Lista: " + lista.getNombreLista(), fuenteLista));
                p2.add(new Phrase(Chunk.NEWLINE));
                p2.add(new Phrase(Chunk.NEWLINE));
                pdf.add(p2);
                if (!lista.getVideos().isEmpty()) {
                    tabla.addCell("Titulo");
                    tabla.addCell("Genero");
                    tabla.addCell("Num Reproducciones");
                    for (Video video : lista.getVideos()) {
                        tabla.addCell(video.getTitulo());
                        tabla.addCell(video.getGenero());
                        String aux = Integer.toString(video.getNumReproducciones());
                        tabla.addCell(aux);
                    }
                }
                pdf.add(tabla);
                Paragraph p3 = new Paragraph();
                p3.add(new Phrase(Chunk.NEWLINE));
                pdf.add(p3);
            }

            pdf.close();
            ficheroSalida.close();
        } catch (IOException | DocumentException e) {
        }
    }

    public boolean registrarUsuario(String nombre, String apellidos, String correo, String username, String password,
                                    LocalDate fechaNacimiento) {
        if (existeUsuario(username)) {
            return false;
        }

        Usuario usuario = new Usuario(nombre, apellidos, correo, username, password, fechaNacimiento);
        usuario.setFechaRegistro(LocalDate.now());
        usuarioDAO.registrarUsuario(usuario);
        CatalogoUsuarios.getUnicaInstancia().creaUsuario(usuario);
        return true;
    }

    public Usuario getUsuarioActual() {
        return this.usuarioActual;
    }

    public void actualizarUsuario(String nombre, String apellidos, String correo, LocalDate fechaNacimiento){
        usuarioActual.setNombre(nombre);
        usuarioActual.setApellidos(apellidos);
        usuarioActual.setEmail(correo);
        usuarioActual.setFechaNacimiento(fechaNacimiento);
        usuarioDAO.modificarUsuario(usuarioActual);
    }

    public void cambiarArchivoComponente(String rutaXML){
        buscadorVideos.setArchivoVideos(rutaXML);
    }

    public void mejorarCuenta(Usuario usuario) {
        usuario.setPremium(true);
        usuarioDAO.modificarUsuario(usuario);
    }

    public void cancelarSuscripcion(Usuario usuario) {
        usuario.setPremium(false);
        usuarioDAO.modificarUsuario(usuario);
    }

    public void setTipoFiltro(Usuario usuario, String filtro){
        usuarioActual.setTipoFiltro(filtro);
        usuarioDAO.modificarUsuario(usuario);
    }

    public List<ListaVideos> recuperarListasUsuario(Usuario usuario) {
        return usuario.getListasReproduccion();
    }

    public void addVideoRecientes(Usuario usuario, Video video) {
        usuario.addRecientes(video);
        usuarioDAO.modificarUsuario(usuario);
    }

    public void addReproduccion(Video video) {
        video.addReproduccion();
        videoDAO.modificarVideo(video);
    }

    public void addVideoVistoUsuario() {
        this.usuarioActual.addVideoVisto();
    }

    public void addEtiquetaVideo(Etiqueta etiqueta, Video video) {
        if(video.addEtiqueta(etiqueta)){
            if(!CatalogoEtiquetas.getUnicaInstancia().existeEtiqueta(etiqueta.getNombre())){
                etiquetaDAO.registrarEtiqueta(etiqueta);
                CatalogoEtiquetas.getUnicaInstancia().creaEtiqueta(etiqueta);
            }
            videoDAO.modificarVideo(video);
        }
    }

    public void removeEtiquetaVideo(Etiqueta etiqueta, Video video) {
        int x = 0;
        if(video.removeEtiqueta(etiqueta)){
            for(Video v : CatalogoVideos.getUnicaInstancia().getVideos()){
                if(v.getEtiquetas().contains(etiqueta)){
                    x=1;
                }
            }
        }
        videoDAO.modificarVideo(video);

        if(x==0) {
            CatalogoEtiquetas.getUnicaInstancia().borraEtiqueta(etiqueta);
            etiquetaDAO.borrarEtiqueta(etiqueta);
        }
    }

    public void addVideoMasVistos(Usuario usuario, Video video) {
        usuario.addVideoMasVistos(video);
        usuarioDAO.modificarUsuario(usuario);
    }

    public List<Video> getMasReproducidosAplicacion(){
        return CatalogoVideos.getUnicaInstancia().getMasReproducidos();
    }

    public List<Video> getVideosRecientes(Usuario usuario) {
        return usuario.getListaRecientes();
    }

    public void actualizarLista(Usuario usuario, String nombreLista, List<Video> videos) {
        ListaVideos lista = usuario.getListaPorNombre(nombreLista);
        lista.setVideos(videos);
        listaVideosDAO.modificarListaVideos(lista);
    }

    public void crearLista(Usuario usuario, String nombreLista, List<Video> videos)  {
        ListaVideos lista = usuario.newLista(nombreLista, videos);
        listaVideosDAO.registrarListaVideos(lista);
        usuario.addListaVideos(lista);
        usuarioDAO.modificarUsuario(usuario);
    }

    public void borrarListaVideos(Usuario usuario, ListaVideos lista) {
        usuario.deleteLista(lista);
        usuarioDAO.modificarUsuario(usuario);
        listaVideosDAO.borrarListaVideos(lista);
    }

    public List<Video> getVideosCatalogo() {
        return CatalogoVideos.getUnicaInstancia().getVideos();
    }

    public Video getVideoSeleccionado() {
        return this.videoSeleccionado;
    }

    public void setVideoSeleccionado(Video video){
        this.videoSeleccionado = video;
    }

    public List<Video> getMasReproducidosUsuario(Usuario usuarioActual) {

        HashMap<Video, Integer> mapa = new HashMap<>(usuarioActual.getMapaMasVistos());
        HashMap<Video, Integer> mapaOrdenado;

        mapaOrdenado = mapa.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey , Map.Entry::getValue , (e1 , e2) -> e2 , LinkedHashMap::new));

        return new LinkedList<>(mapaOrdenado.keySet());
    }

}

